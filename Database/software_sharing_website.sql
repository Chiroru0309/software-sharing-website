-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1
-- Thời gian đã tạo: Th5 17, 2019 lúc 10:34 AM
-- Phiên bản máy phục vụ: 10.1.40-MariaDB
-- Phiên bản PHP: 7.3.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `software_sharing_website`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `binhluan`
--

CREATE TABLE `binhluan` (
  `ID_BinhLuan` int(10) NOT NULL,
  `Noi_dung` text COLLATE utf8_unicode_ci,
  `ID_sanpham` int(10) DEFAULT NULL,
  `ID_user` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `binhluan`
--

INSERT INTO `binhluan` (`ID_BinhLuan`, `Noi_dung`, `ID_sanpham`, `ID_user`) VALUES
(1, 'zing rất tuyệt, tôi rất thích, nhưng có nhiều bài hát hay mà zing chưa cập nhật và không có mục danh sách đã thích làm tôi rất thất vọng.', 1003, 2010),
(2, 'Nhìn chung game rất hay, đơn giản, rất tuyệt vời.', 1004, 2011),
(12250, 'ứng dụng rất tuyệt , tôi đã kết nối được rất nhiều wifi mà mật khẩu rất khó giải', 1005, 2005);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `san_pham`
--

CREATE TABLE `san_pham` (
  `ID_SanPham` int(10) NOT NULL,
  `Ten` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `Soluottai` int(100) NOT NULL,
  `MoTa` text COLLATE utf8_unicode_ci NOT NULL,
  `Loai` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `HinhAnh` text COLLATE utf8_unicode_ci NOT NULL,
  `LinkTai` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `san_pham`
--

INSERT INTO `san_pham` (`ID_SanPham`, `Ten`, `Soluottai`, `MoTa`, `Loai`, `HinhAnh`, `LinkTai`) VALUES
(1001, 'Messenger', 1, 'GỬI TIN NHẮN - Bỏ qua công đoạn trao đổi số điện thoại, chỉ cần gửi tin nhắn. Quá trình này diễn ra liền mạch trên các thiết bị.', 'Liên lạc', 'https://lh3.googleusercontent.com/I9j5jRbLkr5hxxLDN1d_2uj6iZWEtH9cKhRkORcNxPxYjrxMj9eN1Ad2ZNwMnZxFbJo', ''),
(1002, 'Zalo', 6, 'Zalo là ứng dụng nhắn tin kiểu mới và kết nối cộng đồng hàng đầu cho người dùng di động Việt.', 'Liên lạc', 'https://stc-zaloprofile.zdn.vn/pc/v1/images/zalo_sharelogo.png', ''),
(1003, 'Zing MP3', 9, 'Zing MP3 là ứng dụng nghe nhạc miễn phí hàng đầu Việt Nam. Với kho nhạc chất lượng cao đồ sộ', 'Nhạc và Âm thanh', 'https://static-zmp3.zadn.vn/skins/common/logo600.png', ''),
(1004, 'HAGO', 6, 'Chơi trò chơi, Kết bạn.Kết nối trực tiếp với các game thủ từ các địa điểm khác nhau. Hãy vào trận chiến PK, chơi và trò chuyện cùng một lúc!', 'Game', 'https://is3-ssl.mzstatic.com/image/thumb/Purple123/v4/62/cc/db/62ccdbd6-6765-9dd0-44e4-e2e666c6741f/AppIcon-0-1x_U007emarketing-0-0-85-220-0-10.png/1200x630wa.png', ''),
(1005, 'WiFi Chìa khóa vạn năng', 3, 'Wifi Chìa Khoá Vạn Năng là ứng dụng kết nối Wifi công cộng miễn phí, tiện lợi và an toàn dựa trên nền tảng chia sẻ Wifi lớn nhất Thế Giới của Linksure Network', 'Công cụ', 'https://lh3.googleusercontent.com/SOt9c0orI6LOnZudivHOInmRhFBnZtnX6PdI2_gOy1g9R_zzu20hIMf4izN4Ig5c224', ''),
(1006, 'TikTok', 7, 'TikTok là mạng xã hội cực HOT về video nơi mọi người chia sẻ các clip ngắn được truyền cảm hứng bằng âm nhạc.', 'Xem và sửa video', 'https://vnreview.vn/image/18/61/59/1861592.jpg', ''),
(1007, 'Facebook', 3, 'Cập nhật thông tin từ bạn bè nhanh chóng hơn bao giờ hết.', 'Xã hội', 'https://www.facebook.com/images/fb_icon_325x325.png', ''),
(1008, 'Garena Free Fire', 9, 'Free Fire là tựa game bắn súng sinh tốn hot nhất trên mobile. Không mất quá nhiều thời gian cho người chơi, mỗi 10 phút bạn có thể chiến đấu với 49 người chơi khác, tất cả vì mục tiêu tồn tại đến cuối cùng.', 'Game', 'https://image.winudf.com/v2/image1/Y29tLmR0cy5mcmVlZmlyZXRoX3NjcmVlbl82XzE1NTM5NjU2NDZfMDcx/screen-6.jpg?h=800&fakeurl=1', ''),
(1009, 'Shopee', 5, 'SHOPEE là ứng dụng mua sắm trực tuyến hàng đầu tại Việt Nam, Đài Loan và nhiều nước Đông Nam Á khác. Hoàn toàn MIỄN PHÍ cho bạn mua và bán online một cách DỄ DÀNG, NHANH CHÓNG, AN TOÀN với chi phí siêu tiết kiệm.', 'Mua sắm', 'https://upload.wikimedia.org/wikipedia/vi/thumb/a/a2/Shopee_logo%2C_English_versino%2C_Mar_2019.png/220px-Shopee_logo%2C_English_versino%2C_Mar_2019.png', ''),
(1010, 'Stack Ball', 2, 'Stack Ball là một trò chơi điện tử 3d trong đó người chơi đập, va đập và bật qua các nền tảng xoắn ốc xoay tròn để đi tới đích.', 'Game', 'https://i1.silvergames.com/p/b/stack-ball.png', ''),
(1011, 'AOG - Đấu Trường Vinh Quang', 3, 'AOG – Đấu Trường Vinh Quang sử dụng bối cảnh nội dung dựa theo sự tổng hợp của hàng trăm nền văn minh đa dạng trên khắp trái đất tạo thành liên quân công lý, thậm chí đi xa ra cả ngoài vũ trụ và sự tưởng tượng của con người nữa', 'Game', 'https://aog.gamota.com/img/frontend/tao-thiep/logo-aog.png', ''),
(1012, 'Sendo', 6, 'Shopping nhanh chóng với những xu hướng mua sắm quần áo, đồ trang điểm, thiết bị điện tử, đồ công nghệ, các mẫu điện thoại và mỹ phẩm, sách mới nhất đang được hàng triệu khách hàng online quan tâm, bạn dễ dàng lựa chọn từ những shop thời trang, cửa hàng trực tuyến trên Sendo với tính năng Xu Hướng Tìm Kiếm.', 'Mua sắm', 'https://is1-ssl.mzstatic.com/image/thumb/Purple123/v4/66/f2/c0/66f2c041-d3a6-add8-ee8f-b3fff97205e6/AppIcon-0-1x_U007emarketing-0-0-85-220-7.png/246x0w.jpg', ''),
(1013, 'Facebook Lite', 9, 'Giữ liên lạc với bạn bè nhanh hơn bao giờ hết', 'Xã hội', 'https://cdn4.tgdd.vn/Products/Images/1784/71224/FacebookLite-icon.png', ''),
(1014, 'Tiles Hop', 1, 'Với Tiles Hop: EDM Rush!, bạn có thể chơi game với rất nhiều thể loại nhạc hấp dẫn: Piano, Guitar, Rock và nhạc điện tử EDM.', 'Game', 'https://is1-ssl.mzstatic.com/image/thumb/Purple113/v4/21/01/b3/2101b3e3-18c6-bb97-fada-06cac649604d/AppIcon-0-1x_U007emarketing-0-85-220-7.png/246x0w.jpg', ''),
(1015, 'Garena Liên Quân', 5, 'ĐẤU TRƯỜNG HUYỀN THOẠI', 'Game', 'https://f.gxx.garenanow.com/download/8e666a58be8affdba7a7fcd0dc7c5e3e04030100000098af000000000201003f', ''),
(1016, 'Messenger Lite', 0, 'Ứng dụng nhắn tin nhanh và tiết kiệm dữ liệu hỗ trợ bạn liên hệ với mọi người trong cuộc sống của mình', 'Liên lạc', 'https://www.softoco.com/wp-content/uploads/2018/08/Facebook-Messenger-Lite-download-03.png', ''),
(1017, 'Ulike', 6, 'Tinh chỉnh gương mặt theo thời gian thực, hiệu ứng làm đẹp mắt, mũi, miệng được thiết kế tinh tế công phu, không cần chỉnh sửa gì thêm! Hỗ trợ lưu trữ tùy chỉnh của gương mặt mẫu, mỗi lần chụp là một nàng tiên cổ tích', 'Nhiếp ảnh', 'https://lh3.googleusercontent.com/kcy-J6uHCgtjk4xZwUkMlj93hBnza4cIN-YZ_dYiwIcf701OgXwFt__-_i6ZKGLzRYA', ''),
(1018, 'Lazada', 2, 'ứng dụng mua sắm trực tuyến số 1 tại Việt Nam.', 'Mua sắm', 'https://upload.wikimedia.org/wikipedia/commons/1/17/Lazada.vn_Logo.png', ''),
(1019, 'Kick the Buddy', 1, 'Ban bong bong', 'Game', 'https://is4-ssl.mzstatic.com/image/thumb/Purple113/v4/03/02/a1/0302a178-b21b-0c22-f4ad-8eac9926a487/AppIcon_new-0-1x_U007emarketing-0-85-220-0-10.png/246x0w.jpg', ''),
(1020, 'Crowd City', 6, 'Tap trung cu dan', 'Game', 'https://is5-ssl.mzstatic.com/image/thumb/Purple123/v4/3a/4e/66/3a4e66c6-9504-59f2-e846-dbb19d6123a3/AppIcon-0-1x_U007emarketing-0-85-220-0-7.png/246x0w.jpg', ''),
(1021, 'VN Ngày Nay', 2, 'VN Ngày Nay là ứng dụng đọc báo online miễn phí, tin tức nóng, tin tức 24h với nhiều bài báo mới nhất tại Việt Nam, giúp bạn cập nhật tin tức trong và ngoài nước một cách toàn diện nhất. ', 'Tin tức và Tạp chí', 'https://is5-ssl.mzstatic.com/image/thumb/Purple113/v4/e8/12/8c/e8128cc6-c78e-a4aa-8404-b0227e26921e/AppIcon-0-1x_U007emarketing-0-0-85-220-6.png/246x0w.jpg', ''),
(1022, 'PUBG MOBILE VN', 1, 'game bắn súng sinh tồn được yêu thích nhất trên toàn thế giới do Tencent & BlueHole nghiên cứu phát triển đã được phát hành chính thức tại Việt Nam duy nhất bởi VNG', 'Game', 'https://is4-ssl.mzstatic.com/image/thumb/Purple114/v4/d2/a6/a8/d2a6a817-8d1f-cdc6-20e3-44601a7020c4/AppIcon-1x_U007emarketing-0-0-GLES2_U002c0-512MB-sRGB-0-0-0-85-220-0-0-0-8.png/246x0w.jpg', ''),
(1023, 'Anger of stick 5', 6, 'Sử dụng nhiều loại vũ khí, bạn có thể loại bỏ các kẻ thù', 'Game', 'https://cdn6.aptoide.com/imgs/2/d/c/2dcaad4cb62abd5b76419568a81c49d3_icon.png?w=240', ''),
(1024, 'Cổng game ZingPlay', 2, 'Cổng game giải trí ZingPlay là cổng game giải trí đa nền tảng đầu tiên ra mắt tại Việt Nam', 'Game', 'https://i2.wp.com/ioshay.com/wp-content/uploads/2018/09/zingplay-game-bai-game-co-1.0-ios.jpg?fit=600%2C600&ssl=1', ''),
(1025, 'Đao Kiếm Vô Song', 3, 'Đao Kiếm Vô Song là tựa game Võ Hiệp với đồ họa full 3D tuyệt đỉnh dành cho các game thủ yêu thích game nhập vai online.', 'Game', 'https://ttpgame.com/styles/home/games/game-1/images/baokiem.png', ''),
(1026, 'TIKI', 2, 'App TIKI giúp bạn mua sắm shopping tiện lợi với ƯU ĐÃI GIÁ TỐT, đảm bảo HÀNG CHÍNH HÃNG chất lượng cao & GIAO HÀNG NHANH TIKINOW.', 'Mua sắm', 'https://cdn-images-1.medium.com/max/1200/1*PKDcYE819UPQgMXhYE_sZQ@2x.png', ''),
(1027, 'Grab', 4, 'Grab kết nối bạn với Đối tác Tài xế nhằm mang đến cho bạn những chuyến đi thoải mái với chi phí phù hợp', 'Bản đồ và dẫn đường', 'https://media.licdn.com/dms/image/C4E0BAQGMv0-OtFdbYw/company-logo_200_200/0?e=2159024400&v=beta&t=ToPRqZxMJgx2hSu_UBbcSPxmLwhbuikB2eZMzLuxq5k', ''),
(1028, 'BÁO MỚI', 5, 'Báo Mới là ứng dụng đọc báo chính thức của BaoMoi.com - trang tổng hợp tin tức tự động hàng đầu Việt Nam. Sử dụng công nghệ máy tính thông minh độc quyền của BaoMoi.com, ứng dụng cho phép bạn đọc nhanh các tin tức nóng và mới nhất trong ngày được chọn lọc từ hơn 150 báo điện tử tại Việt Nam.', 'Tin tức và Tạp chí', 'https://yt3.ggpht.com/a-/AAuE7mBFInCibrJvLDBd0S0URS0G5IiBABVu1RY1SA=s900-mo-c-c0xffffffff-rj-k-no', ''),
(1029, 'NhacCuaTui', 3, 'NhacCuaTui tiện dụng hơn cho trải nghiệm âm nhạc của bạn - Dù bạn đang ở đâu, dù bạn đang làm gì, hãy để NhacCuaTui đồng hành cùng bạn.', 'Giải trí', 'https://is1-ssl.mzstatic.com/image/thumb/Purple124/v4/23/63/dc/2363dc60-8947-bf84-c13f-f6010980ae5f/source/512x512bb.jpg', ''),
(1030, 'Momo', 10, 'Với Ví điện tử MoMo, thanh toán &amp; chuyển tiền trên di động tiện lợi, an toàn, nhiều ưu đãi hơn bao giờ hết! Quên đi phiền toái của tiền lẻ, quẹt thẻ &amp; chuyển tiền rườm rà!', 'Tài chính', 'https://static.mservice.io/img/momo-upload-api-logo-momo-181015105508.png', '');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `su_dung`
--

CREATE TABLE `su_dung` (
  `ID_User` int(10) NOT NULL,
  `Username` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `Pass_word` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `FName` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `LName` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `Email` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `Quyen` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `Avatar` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `su_dung`
--

INSERT INTO `su_dung` (`ID_User`, `Username`, `Pass_word`, `FName`, `LName`, `Email`, `Quyen`, `Avatar`) VALUES
(2001, 'Khoa95', '123456', 'Ta', 'Khoa', 'Khoa95@gmail.com', 'Admin', 'http://thuthuat123.com/uploads/2018/01/27/Avatar-dep-nhat-2_112147.jpg'),
(2002, 'hangdethuong', '456789', 'Ta', 'Khoa', 'hangcute@yahoo.vn', 'Admin', 'http://thuthuatphanmem.vn/uploads/2018/06/18/anh-avatar-dep-44_034122130.jpg'),
(2003, 'thaituan', 'thai423', 'Ta', 'Khoa', 'hoahong679@gmail.com', 'Customer', 'https://thuthuatnhanh.com/wp-content/uploads/2018/07/anh-avatar-dep.jpg'),
(2004, 'zindeptrai', 'hihihaha', 'Ta', 'Khoa', 'zinzin1@yahoo.com', 'Customer', 'http://thuthuat123.com/uploads/2018/01/27/Avatar-dep-nhat-75_112148.jpg'),
(2005, 'quatangcuocsong', 'quatang', 'Ta', 'Khoa', 'quatangdethuong@gmail.com', 'Customer', 'http://quatangabc.com/images/hinhsinhnhat/avatar1.jpg'),
(2006, 'hinhdep893', 'avatar', 'Ta', 'Khoa', 'hinhbaodep@yahoo.com.vn', 'Customer', 'https://tophinhanhdep.com/wp-content/uploads/2017/07/hinh-avatar-dep-de-thuong-cute.jpg'),
(2007, 'thuthuat', 'phanmem', 'Ta', 'Khoa', 'thuthuatphanmem@zing.vn', 'Customer', 'http://thuthuatphanmem.vn/uploads/2018/04/10/avatar-doi-dep-1_015655450.jpg'),
(2008, 'wiki', 'wikicachlam', 'Ta', 'Khoa', 'wiki456@yahoo.vn', 'Customer', 'https://wikicachlam.com/wp-content/uploads/2018/05/avatar-facebook-dep.jpg'),
(2009, 'avaimg', 'imgava', 'Ta', 'Khoa', 'imgdong@gmail.com', 'Customer', 'https://i.pinimg.com/236x/a6/df/da/a6dfdaf75ceeb010352e3df2fc643456.jpg'),
(2010, 'hinhanh', 'anh12dep', 'Ta', 'Khoa', 'hinhanhava@yahoo.vn', 'Customer', 'https://tophinhanhdep.com/wp-content/uploads/2017/10/anh-cap-dep-doi-238x300.jpg'),
(2011, 'congdong', 'nhac175', 'Ta', 'Khoa', 'congdongnhac@zing.vn', 'Customer', 'https://cdn.trangcongnghe.com/uploads/posts/2017-01/moi-tai-ve-bo-avatar-pikagon-rong-lai-pokemon-sieu-cute_2.jpeg');

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `binhluan`
--
ALTER TABLE `binhluan`
  ADD PRIMARY KEY (`ID_BinhLuan`),
  ADD KEY `ID_sanpham` (`ID_sanpham`),
  ADD KEY `ID_user` (`ID_user`);

--
-- Chỉ mục cho bảng `san_pham`
--
ALTER TABLE `san_pham`
  ADD PRIMARY KEY (`ID_SanPham`);

--
-- Chỉ mục cho bảng `su_dung`
--
ALTER TABLE `su_dung`
  ADD PRIMARY KEY (`ID_User`);

--
-- Các ràng buộc cho các bảng đã đổ
--

--
-- Các ràng buộc cho bảng `binhluan`
--
ALTER TABLE `binhluan`
  ADD CONSTRAINT `binhluan_ibfk_1` FOREIGN KEY (`ID_sanpham`) REFERENCES `san_pham` (`ID_SanPham`),
  ADD CONSTRAINT `binhluan_ibfk_2` FOREIGN KEY (`ID_user`) REFERENCES `su_dung` (`ID_User`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
