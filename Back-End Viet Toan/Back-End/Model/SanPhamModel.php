<?php
require_once 'Crud.php';
class SoftwareModel 
{
	var $crud;		
	function __construct()
	{
		$this->crud = new Crud();
	}

	function select_sanpham(){
		$sql = 'SELECT * FROM san_pham WHERE DaXoa = 0';
		return $this->crud->getData($sql);
	}

	function insert_sanpham($data){
		$sql = "INSERT INTO san_pham(Ten,HinhAnh,MoTa,Loai,LinkTai) 
		VALUES('{$data->Ten}','{$data->HinhAnh}','{$data->MoTa}','{$data->Loai}','{$data->LinkTai}')";

		return $this->crud->execute($sql);
	}

	function delete_sanpham($data){
		$sql = "UPDATE san_pham SET DaXoa=1 
		WHERE username='{$data->ID_SanPham}'";

		return $this->crud->execute($sql);	
	}

	function update_sanpham($data)
	{
		$sql = "UPDATE san_pham SET Ten='{$data->Ten}', MoTa = '{$data->MoTa}'
		Loai = '{$data->Loai}', HinhAnh = '{$data->HinhAnh}', LinkTai = '{$data->LinkTai}'
		WHERE ID_SanPham = {$data->ID_SanPham} AND DaXoa=0" ;

		return $this->crud->execute($sql);	
	}
}
?>