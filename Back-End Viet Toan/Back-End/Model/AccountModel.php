<?php
require_once 'Crud.php';
class AccountModel
{
	var $crud;
	function __construct()
	{
		$this->crud = new Crud();
	}

	function select_acount($username){
		$sql = "SELECT * FROM Su_Dung WHERE Username = '{$username}' AND DaXoa = 0";
		if ($this->crud->getData($sql)){
			return (object)$this->crud->getData($sql)[0];
		}
		else{
			return new stdClass();
		}
	}

	function select_all_acount()
	{
		$sql = "SELECT * FROM Su_Dung WHERE DaXoa = 0";
		if ($this->crud->getData($sql)){
			return (object)$this->crud->getData($sql)[0];
		}
		else{
			return new stdClass();
		}
	}

	function insert_account($data){
		$sql = "INSERT INTO Su_Dung(Username,Email,Pass_word,FName,LName,Quyen) VALUES('{$data->username}',
			'{$data->email}','{$data->password}','{$data->FName}','{$data->LName}','Customer')";

		return $this->crud->execute($sql);
	}

	function change_account($data){
		$sql = "UPDATE Su_Dung SET Email='{$data->email}'".
				isset($data->password)?",Pass_word='{$data->password}'":"". 
				",Avatar='{$data->sourceAvatar}
				,FName='{$data->FName}
				,LName='{$data->LName} 
				WHERE Username='{$data->username}'";

		return $this->crud->execute($sql);
	}

	function delete_account($data){
		$sql = "UPDATE Su_Dung SET DaXoa=1 
				WHERE Username='{$data->username}'";
		return $this->crud->execute($sql);
	}
}
?>