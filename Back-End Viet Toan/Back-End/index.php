<?php
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
header("content-type: text/html; charset=UTF-8"); 
//error_reporting(0);



require_once "./Controller/RegisterController.php";
require_once "./Controller/LoginController.php";
require_once "./Controller/SanphamController.php";
require_once "./Controller/CommentController.php";
require_once "./Controller/SanPhamAdminController.php";
require_once "./Controller/AccountAdminController.php";


if (isset($_GET['controller'])){
	$controller = $_GET['controller'];

	switch ($controller) {
		case 'Register':
			$Controller = new RegisterController();
			break;
		case 'Login' :
			$Controller = new LoginController();
			break;
		case 'Sanpham':
			$Controller = new SanphamController();
			break;
		case 'Comment':
			$Controller = new CommentController();
			break;
		case 'SanPhamAdmin':
			$Controller = new SanPhamAdminController();
			break;
		case 'AccountAdmin':
			$Controller = new AccountAdminController();
			break;
		default:
			break;
	}

	$Controller->{$_GET['action']}();

}

?>