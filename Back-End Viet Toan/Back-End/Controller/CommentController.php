<?php
require_once './Model/CommentModel.php';

class CommentController{
    function __construct()
	{
        $this->model = new CommentModel();
    }

    function select_comment(){
        $model = new CommentModel();
        echo json_encode($model->select_comment(),JSON_UNESCAPED_UNICODE);
    }

    function update_comment()
	{
		try{
			$response = new stdClass();
			$data = json_decode(file_get_contents("php://input"));
			$response->success = $this->model->update_comment($data)?1:0;
			$response->mess =  $response->success == 1 ? "": "Không cập nhật bình luận được.";
			
		}
		catch(Exception $e){
			$response->success = 0;
			$response->mess = $e->getMessage();
		} 

		echo json_encode($response,JSON_UNESCAPED_UNICODE);
	}

	function insert_comment()
	{
		try{
			$response = new stdClass();
			$data = json_decode(file_get_contents("php://input"));
			$response->success = $this->model->insert_comment($data)?1:0;
			$response->mess =  $response->success == 1 ? "": "Không bình luận được.";
		}
		catch(Exception $e){
			$response->success = 0;
			$response->mess = $e->getMessage();
		} 

		echo json_encode($response,JSON_UNESCAPED_UNICODE);
	}

	function delete_comment()
	{
		try{
			$response = new stdClass();
			$data = json_decode(file_get_contents("php://input"));
			$model = new SanphamModel();
			$response->success = $this->model->delete_comment($data)?1:0;
			$response->mess =  $response->success == 1 ? "": "Không xóa bình luận được";
			
		}
		catch(Exception $e){
			$response->success = 0;
			$response->mess = $e->getMessage();
		} 

		echo json_encode($response,JSON_UNESCAPED_UNICODE);
	}
}
?>