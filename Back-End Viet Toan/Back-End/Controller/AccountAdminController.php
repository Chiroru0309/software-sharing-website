<?php

require_once './Model/AccountModel.php';
require_once './Controller/RegisterController.php';

class AccountAdminController{
    function __contruct()
    {
        $this->model = new AccountModel();
    }

    function insert_account()
	{
        $model = new RegisterController();
        $model->sign_up();
    }
    
    function update_account(){
        try{
			$response = new stdClass();
			$data = json_decode(file_get_contents("php://input"));
			$data->password = password_hash($data->password, PASSWORD_DEFAULT);

			$model = new AccountModel();	
			if (!$model->change_account($data)){
				throw new Exception("Email bị trùng.");
			}
			$response->success = 1;
			$response->mess = "";
		}
		catch(Exception $e){
			$response->success = 0;
			$response->mess = $e->getMessage();
		}

		echo json_encode($response,JSON_UNESCAPED_UNICODE);
    }

    function delete_account()
	{
		try{
			$response = new stdClass();
			$data = json_decode(file_get_contents("php://input"));
			$model = new SanphamModel();
			$response->success = $this->model->delete_account($data)?1:0;
			$response->mess =  $response->success == 1 ? "": "Không xóa tài khoản được";
			
		}
		catch(Exception $e){
			$response->success = 0;
			$response->mess = $e->getMessage();
		} 

		echo json_encode($response,JSON_UNESCAPED_UNICODE);
    }
    
    function select_account(){
       
        echo json_encode($this->model->select_all_acount(),JSON_UNESCAPED_UNICODE);
    }
}
?>
