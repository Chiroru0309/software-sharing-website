<?php
require_once './Model/SanPhamModel.php';
class SanPhamAdminController
{
	
	function __construct()
	{
		
	}

	function update_sanpham()
	{
		try{
			$response = new stdClass();
			$data = json_decode(file_get_contents("php://input"));
			$model = new SanphamModel();
			$response->success = $model->update_sanpham($data)?1:0;
			$response->mess =  $response->success == 1 ? "": "Không cập nhật sản phẩm được";
			
		}
		catch(Exception $e){
			$response->success = 0;
			$response->mess = $e->getMessage();
		} 

		echo json_encode($response,JSON_UNESCAPED_UNICODE);
	}

	function insert_sanpham()
	{
		try{
			$response = new stdClass();
			$data = json_decode(file_get_contents("php://input"));
			$model = new SanphamModel();
			$response->success = $model->insert_sanpham($data)?1:0;
			$response->mess =  $response->success == 1 ? "": "Tên sản phẩm bị trùng.";
			
		}
		catch(Exception $e){
			$response->success = 0;
			$response->mess = $e->getMessage();
		} 

		echo json_encode($response,JSON_UNESCAPED_UNICODE);
	}

	function delete_sanpham()
	{
		try{
			$response = new stdClass();
			$data = json_decode(file_get_contents("php://input"));
			$model = new SanphamModel();
			$response->success = $model->delete_sanpham($data)?1:0;
			$response->mess =  $response->success == 1 ? "": "Không xóa sản phẩm được";
			
		}
		catch(Exception $e){
			$response->success = 0;
			$response->mess = $e->getMessage();
		} 

		echo json_encode($response,JSON_UNESCAPED_UNICODE);
	}
}
?>