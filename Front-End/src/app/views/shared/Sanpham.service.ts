import { Injectable } from '@angular/core';
import {Http, Response,  Headers, RequestOptions, RequestMethod} from '@angular/http';
import {sanpham} from '../models/sanpham.class';
import { Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {HttpHeaders} from '@angular/common/http';
import { Options } from 'selenium-webdriver/firefox';
const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};



@Injectable({
  providedIn: 'root'
})
export class SanPhamService {

  API_URL: string = 'http://localhost:8012/api/product/read.php';
  constructor( public http: Http) { }
  getsanphamsSmall() {
    return this.http.get(this.API_URL);
  }

  putsanphams(sp: sanpham): Observable<sanpham>{
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
   return this.http.put('http://localhost:8012/api/product/update.php', sp,options).pipe(map((res: Response) => res.json()));
  }

  postsanphams(sp: sanpham){
   // return this.http.post(`${this.API_URL}/${sp.id}`,sp);
  }

  deletesanphams(ID_SanPham):Observable<sanpham>{
    let headers = new Headers({'Content-Type':'application/json'});
    let options = new RequestOptions({headers: headers});
    return this.http.post('http://localhost:8012/api/product/delete.php',{id:ID_SanPham},options).pipe(map((res:Response) => res.json()));
  }
}
