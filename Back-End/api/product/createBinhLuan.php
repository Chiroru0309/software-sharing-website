<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods:  POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
 
// get database connection
include_once '../config/database.php';
 
// instantiate BinhLuan object
include_once '../objects/BinhLuan.php';
 
$database = new Database();
$db = $database->getConnection();
 
$CMT = new BinhLuan($db);
 
// get posted data
$data = json_decode(file_get_contents("php://input"));
 
// make sure data is not empty
// if(
//     !empty($data->Ten) &&
//     !empty($data->Soluottai) &&
//     !empty($data->MoTa) &&
//     !empty($data->ID_user) &&
//     !empty($data->ID_BinhLuan)
// ){
 
    // set BinhLuan property values
    $CMT->ID_user = $data->ID_user;
    $CMT->ID_sanpham = $data->ID_sanpham;
    $CMT->Noi_dung = $data->Noi_dung;
    $CMT->ID_BinhLuan = $data->ID_BinhLuan;
    //$CMT->created = date('Y-m-d H:i:s');
 
    // create the BinhLuan
    if($CMT->create()){
 
        // set response code - 201 created
        http_response_code(201);
 
        // tell the user
        echo json_encode(array("message" => "BinhLuan was created."));
    }
 
    // if unable to create the BinhLuan, tell the user
    else{
 
        // set response code - 503 service unavailable
        http_response_code(503);
 
        // tell the user
        echo json_encode(array("message" => "Unable to create BinhLuan."));
    }
// }
 
// // tell the user data is incomplete
// else{
 
//     // set response code - 400 bad request
//     http_response_code(400);
 
//     // tell the user
//     echo json_encode(array("message" => "Unable to create BinhLuan. Data is incomplete."));
// }
// ?>