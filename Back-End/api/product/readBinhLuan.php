<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

// include database and object files
include_once '../config/database.php';
include_once '../objects/BinhLuan.php';
 
// instantiate database and BinhLuan object
$database = new Database();
$db = $database->getConnection();
 
// initialize object
$BinhLuan = new BinhLuan($db);
 
// query BinhLuans
$stmt = $BinhLuan->read();
$num = $stmt->rowCount();
 
// check if more than 0 record found
if($num>0){
 
    // BinhLuans array
    $BinhLuans_arr=array();

 
    // retrieve our table contents
    // fetch() is faster than fetchAll()
    // http://stackoverflow.com/questions/2770630/pdofetchall-vs-pdofetch-in-a-loop
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
        // extract row
        // this will make $row['name'] to
        // just $name only
        extract($row);
 
        $BinhLuan_item=array(
            "ID_BinhLuan" => $ID_BinhLuan,
            "Noi_dung" => $Noi_dung,
            "ID_sanpham" => $ID_sanpham,
            "ID_user" => $ID_user

        );
 
        array_push($BinhLuans_arr, $BinhLuan_item);
    }
 
    // set response code - 200 OK
    http_response_code(200);
 
    // show BinhLuans data in json format
    echo json_encode($BinhLuans_arr);
}
 
else{
 
    // set response code - 404 Not found
    http_response_code(404);
 
    // tell the user no BinhLuans found
    echo json_encode(
        array("message" => "No BinhLuans found.")
    );
}

