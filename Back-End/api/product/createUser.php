<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods:  POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
 
// get database connection
include_once '../config/database.php';
 
// instantiate product object
include_once '../objects/user.php';
    
$database = new Database();
$db = $database->getConnection();
 
$User = new User($db);
 
// get posted data
$data = json_decode(file_get_contents("php://input"));
  
    // set product property values
    $User->Quyen = $data->Quyen;
    $User->Email = $data->Email;
    $User->Avatar = $data->Avatar;
    $User->Username= $data->Username;
    $User->Pass_word = $data->Pass_word;
    $User->FName = $data->FName;
    $User->LName = $data->LName;
    $User->ID_User = $data->ID_User;
		
    //User->created = date('Y-m-d H:i:s');
 
    // create the user
    if($User->create()){
 
        // set response code - 201 created
        http_response_code(201);
 
        // tell the user
        echo json_encode(array("message" => "User was created."));
    }
 
    // if unable to create the user, tell the user
    else{
 
        // set response code - 503 service unavailable
        http_response_code(503);
 
        // tell the user
        echo json_encode(array("message" => "Unable to create user."));
    }
// }
 
// // tell the user data is incomplete
// else{
 
//     // set response code - 400 bad request
//     http_response_code(400);
 
//     // tell the user
//     echo json_encode(array("message" => "Unable to create product. Data is incomplete."));
// }
// ?>