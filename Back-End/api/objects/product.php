<?php
class Product{
 
    // database connection and table name
    private $conn;
    private $table_name = "san_pham";
 
    // object properties
    public $ID_SanPham;
    public $Ten;
    public $Soluottai;
    public $MoTa;
    public $Loai;
    public $HinhAnh;
    public $LinkTai;
    // constructor with $db as database connection
    public function __construct($db){
        $this->conn = $db;
    }

    // read products
    function read(){
    
        // select all query
        $query = "SELECT
                    *
                FROM
                    " . $this->table_name . " 
	  ";
    
        // prepare query statement
        $stmt = $this->conn->prepare($query);
    
        // execute query
        $stmt->execute();
    
        return $stmt;
    }

    // update the product
function update(){
 
    // update query
    $query = "UPDATE
                " . $this->table_name . "
            SET
                Ten = :Ten,
                Soluottai = :Soluottai,
                MoTa = :MoTa,
                Loai = :Loai,
                HinhAnh = :HinhAnh,
                LinkTai = :LinkTai
            WHERE
                ID_SanPham = :ID_SanPham;";
 
    // prepare query statement
    $stmt = $this->conn->prepare($query);
 
    // sanitize
    $this->Ten=htmlspecialchars(strip_tags($this->Ten));
    $this->Soluottai=htmlspecialchars(strip_tags($this->Soluottai));
    $this->MoTa=htmlspecialchars(strip_tags($this->MoTa));
    $this->Loai=htmlspecialchars(strip_tags($this->Loai));
    $this->HinhAnh=htmlspecialchars(strip_tags($this->HinhAnh));
    $this->LinkTai=htmlspecialchars(strip_tags($this->LinkTai));
    $this->ID_SanPham=htmlspecialchars(strip_tags($this->ID_SanPham));
 
    // bind new values
    $stmt->bindParam(':Ten', $this->Ten);
    $stmt->bindParam(':Soluottai', $this->Soluottai);
    $stmt->bindParam(':MoTa', $this->MoTa);
    $stmt->bindParam(':Loai', $this->Loai);
    $stmt->bindParam(':HinhAnh', $this->HinhAnh);
    $stmt->bindParam(':LinkTai', $this->LinkTai);
    $stmt->bindParam(':ID_SanPham', $this->ID_SanPham);
 
    // execute the query
    if($stmt->execute()){
        return true;
    }
 
    return false;
}

// create product
function create(){
 
    // query to insert record
    $query = "INSERT INTO
                " . $this->table_name . "
            SET
                Ten=:Ten,
                Soluottai=:Soluottai,
                MoTa=:MoTa,
                Loai=:Loai,
                HinhAnh=:HinhAnh,
                LinkTai=:LinkTai,
                ID_SanPham=:ID_SanPham";
                // created=:created;";
 
    // prepare query
    $stmt = $this->conn->prepare($query);
 
    // sanitize
    $this->Ten=htmlspecialchars(strip_tags($this->Ten));
    $this->Soluottai=htmlspecialchars(strip_tags($this->Soluottai));
    $this->MoTa=htmlspecialchars(strip_tags($this->MoTa));
    $this->Loai=htmlspecialchars(strip_tags($this->Loai));
    $this->HinhAnh=htmlspecialchars(strip_tags($this->HinhAnh));
    $this->LinkTai=htmlspecialchars(strip_tags($this->LinkTai));
    $this->ID_SanPham=htmlspecialchars(strip_tags($this->ID_SanPham));
    // $this->created=htmlspecialchars(strip_tags($this->created));
 
    // bind values
    $stmt->bindParam(":Ten", $this->Ten);
    $stmt->bindParam(":Soluottai", $this->Soluottai);
    $stmt->bindParam(":MoTa", $this->MoTa);
    $stmt->bindParam(":Loai", $this->Loai);
    $stmt->bindParam(":HinhAnh", $this->HinhAnh);
    $stmt->bindParam(":LinkTai", $this->LinkTai);
    $stmt->bindParam(":ID_SanPham", $this->ID_SanPham);
    // $stmt->bindParam(":created", $this->created);
 
    // execute query
    if($stmt->execute()){
        return true;
    }
 
    return false;
     
}

// delete the product
function delete(){
 
    // delete query
    $query = "DELETE FROM " . $this->table_name . " WHERE ID_SanPham = :ID_SanPham;";
 
    // prepare query
    $stmt = $this->conn->prepare($query);
 
    // sanitize
    $this->ID_SanPham=htmlspecialchars(strip_tags($this->ID_SanPham));
 
    // bind id of record to delete
    $stmt->bindParam(':ID_SanPham', $this->ID_SanPham);
 
    // execute query
    if($stmt->execute()){
        return true;
    }
 
    return false;
     
}
}