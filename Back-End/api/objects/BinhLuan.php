<?php
class BinhLuan{
 
    // database connection and table name
    private $conn;
    private $table_name = "BinhLuan";
 
    // object properties
    public $ID_BinhLuan;
    public $Noi_dung;
    public $ID_sanpham;
    public $ID_user;
    // constructor with $db as database connection
    public function __construct($db){
        $this->conn = $db;
    }

    // read BinhLuan 
    function read(){
    
        // select all query
        $query = "SELECT
                    *
                FROM
                    " . $this->table_name . " 
	  ";
    
        // prepare query statement
        $stmt = $this->conn->prepare($query);
    
        // execute query
        $stmt->execute();
    
        return $stmt;
    }

    // update the BinhLuan 
    function update(){
 
    // update query
    $query = "UPDATE
                " . $this->table_name . "
            SET
                ID_sanpham = :ID_sanpham,
                ID_user = :ID_user,
                Noi_dung = :Noi_dung,
            WHERE
                ID_BinhLuan = :ID_BinhLuan;";
 
    // prepare query statement
    $stmt = $this->conn->prepare($query);
 
    // sanitize
    $this->ID_BinhLuan=htmlspecialchars(strip_tags($this->ID_BinhLuan));
    $this->ID_sanpham=htmlspecialchars(strip_tags($this->ID_sanpham));
    $this->ID_user=htmlspecialchars(strip_tags($this->ID_user));
    $this->Noi_dung=htmlspecialchars(strip_tags($this->Noi_dung));
   
    // bind new values
    $stmt->bindParam(':Ten', $this->Ten);
    $stmt->bindParam(':ID_user', $this->ID_user);
    $stmt->bindParam(':Noi_dung', $this->Noi_dung);
    $stmt->bindParam(':ID_BinhLuan', $this->ID_BinhLuan);
 
    // execute the query
    if($stmt->execute()){
        return true;
    }
 
    return false;
}

// create BinhLuan
 function create(){
 
    // query to insert record
    $query = "INSERT INTO
                " . $this->table_name . "
            SET
                ID_BinhLuan=:ID_BinhLuan,
                ID_sanpham=:ID_sanpham,
                ID_user=:ID_user,
                Noi_dung=:Noi_dung
               ;";
                // created=:created;";
 
    // prepare query
    $stmt = $this->conn->prepare($query);
 
    // sanitize
    $this->ID_sanpham=htmlspecialchars(strip_tags($this->ID_sanpham));
    $this->ID_user=htmlspecialchars(strip_tags($this->ID_user));
    $this->Noi_dung=htmlspecialchars(strip_tags($this->Noi_dung));
    $this->ID_BinhLuan=htmlspecialchars(strip_tags($this->ID_BinhLuan));
    // $this->created=htmlspecialchars(strip_tags($this->created));
 
    // bind values
    $stmt->bindParam(":ID_sanpham", $this->ID_sanpham);
    $stmt->bindParam(":ID_user", $this->ID_user);
    $stmt->bindParam(":Noi_dung", $this->Noi_dung);
    $stmt->bindParam(":ID_BinhLuan", $this->ID_BinhLuan);
    // $stmt->bindParam(":created", $this->created);
 
    // execute query
    if($stmt->execute()){
        return true;
    }
 
    return false;
     
}

// delete the BinhLuan 
function delete(){
 
    // delete query
    $query = "DELETE FROM " . $this->table_name . " WHERE ID_BinhLuan = :ID_BinhLuan;";
 
    // prepare query
    $stmt = $this->conn->prepare($query);
 
    // sanitize
    $this->ID_BinhLuan=htmlspecialchars(strip_tags($this->ID_BinhLuan));
 
    // bind id of record to delete
    $stmt->bindParam(':ID_BinhLuan', $this->ID_BinhLuan);
 
    // execute query
    if($stmt->execute()){
        return true;
    }
 
    return false;
     
}
}