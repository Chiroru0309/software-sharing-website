<?php
class User{
 
    // database connection and table name
    private $conn;
    private $table_name = "su_dung";
 
    // object properties
    public $ID_User;
    public $Username;
    public $Pass_word;
    public $FName;
    public $LName;
    public $Avatar;
    public $Quyen;
    public $Email;
 
    // constructor with $db as database connection
    public function __construct($db){
        $this->conn = $db;
    }

    // read products
    function read(){
    
        // select all query
        $query = "SELECT
                    *
                FROM
                    " . $this->table_name . " 
	  ";
    
        // prepare query statement
        $stmt = $this->conn->prepare($query);
    
        // execute query
        $stmt->execute();
    
        return $stmt;
    }

    // update the product
function update(){
 
    // update query
    $query = "UPDATE
                " . $this->table_name . "
            SET
                Email = :Email,
                Quyen = :Quyen,
                Avatar = :Avatar,
                Username = :Username,
                Pass_word = :Pass_word,
                FName = :FName,
                LName = :LName
            WHERE
                ID_User = :ID_User;";
 
    // prepare query statement
    $stmt = $this->conn->prepare($query);
 
    // sanitize
    $this->Email=htmlspecialchars(strip_tags($this->Email));
    $this->Quyen=htmlspecialchars(strip_tags($this->Quyen));
    $this->Avatar=htmlspecialchars(strip_tags($this->Avatar));
    $this->Username=htmlspecialchars(strip_tags($this->Username));
    $this->BinhLuan=htmlspecialchars(strip_tags($this->BinhLuan));
    $this->Pass_word=htmlspecialchars(strip_tags($this->Pass_word));
    $this->FName=htmlspecialchars(strip_tags($this->FName));
    $this->LName=htmlspecialchars(strip_tags($this->LName));
    $this->ID_User=htmlspecialchars(strip_tags($this->ID_User));
 
    // bind new values
    $stmt->bindParam(':Email', $this->Email);
    $stmt->bindParam(':Quyen', $this->Quyen);
    $stmt->bindParam(':Avatar', $this->Avatar);
    $stmt->bindParam(':Username', $this->Username);
    $stmt->bindParam(':Pass_word', $this->Pass_word);
    $stmt->bindParam(':FName', $this->Pass_word);
    $stmt->bindParam(':LName', $this->Pass_word);
    $stmt->bindParam(':ID_User', $this->ID_User);
 
    // execute the query
    if($stmt->execute()){
        return true;
    }
 
    return false;
}

// create product
function create(){
 
    // query to insert record
    $query = "INSERT INTO
                " . $this->table_name . "
            SET
                Email=:Email,
                Quyen=:Quyen,
                Avatar=:Avatar,
                Username=:Username,
                Pass_word=:Pass_word,
                FName = :FName,
                LName = :LName,
                ID_User=:ID_User";
                // created=:created;";
 
    // prepare query
    $stmt = $this->conn->prepare($query);
 
    // sanitize
    $this->Email=htmlspecialchars(strip_tags($this->Email));
    $this->Quyen=htmlspecialchars(strip_tags($this->Quyen));
    $this->Avatar=htmlspecialchars(strip_tags($this->Avatar));
    $this->Username=htmlspecialchars(strip_tags($this->Username));
    $this->FName=htmlspecialchars(strip_tags($this->FName));
    $this->LName=htmlspecialchars(strip_tags($this->LName));
    $this->Pass_word=htmlspecialchars(strip_tags($this->Pass_word));
    $this->ID_User=htmlspecialchars(strip_tags($this->ID_User));
    // $this->created=htmlspecialchars(strip_tags($this->created));
 
    // bind values
    $stmt->bindParam(":Email", $this->Email);
    $stmt->bindParam(":Quyen", $this->Quyen);
    $stmt->bindParam(":Avatar", $this->Avatar);
    $stmt->bindParam(":Username", $this->Username);
    $stmt->bindParam(":Pass_word", $this->Pass_word);
    $stmt->bindParam(":FName", $this->FName);
    $stmt->bindParam(":LName", $this->LName);
    $stmt->bindParam(":ID_User", $this->ID_User);
    // $stmt->bindParam(":created", $this->created);
 
    // execute query
    if($stmt->execute()){
        return true;
    }
 
    return false;
     
}

// delete the product
function delete(){
 
    // delete query
    $query = 
    "DELETE FROM BinhLuan WHERE ID_user = :ID_User;
    DELETE FROM " . $this->table_name . "
     WHERE ID_User = :ID_User;";
 
    // prepare query
    $stmt = $this->conn->prepare($query);
 
    // sanitize
    $this->ID_User=htmlspecialchars(strip_tags($this->ID_User));
 
    // bind id of record to delete
    $stmt->bindParam(':ID_User', $this->ID_User);
 
    // execute query
    if($stmt->execute()){
        return true;
    }
 
    return false;
     
}
}