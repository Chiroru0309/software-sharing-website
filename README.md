﻿## Required

``` bash
node >= 8.9.4
npm > 5.6.0
ng >= 6.1.3  
xampp >= 3.2.3

Back-End:
...
-  Vào trong thư mục Back-End: Copy thư mục api
-  Paste vào đường dẫn C:\xampp\htdoc\
-  Vào trong thư mục Back-End Viet Toan: Copy thư mục Back-End
-  Vào đường dẫn C:\xampp\htdoc\ tạo thư mục Software-sharing-website
-  Paste vào thư mục Software-sharing-website
-  Bật xampp chạy 2 server Apache và MySQL
-  Vào localhost/phpmyadmin rồi tạo 1 database có ngôn ngữ utf8/unicode
-  Copy câu lệnh sql trong thư mục databse rồi tạo các bảng vầ chèn dữ liệu.



Front-End :

```
## Cài đặt

# di chuyển vào trong app's directory
$ cd Software-sharing-website
$ cd Front-End

# Cài các gói cần thiết
$ npm install
```

## Sử dụng

``` bash
# khởi chạy với localhost:4200.
$ npm start --o
# khởi chạy với port ***.
$ npm start --o --port ***
# build cho product
$ ng build
```